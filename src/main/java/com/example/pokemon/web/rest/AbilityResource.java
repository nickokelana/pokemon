package com.example.pokemon.web.rest;

import com.example.pokemon.constant.Constant;
import com.example.pokemon.service.AbilityService;
import com.example.pokemon.service.dto.AbilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("api/abilities")
public class AbilityResource {

    @Autowired
    AbilityService abilityService;

    @GetMapping()
    public ResponseEntity<Object> getAbility(Pageable pageable) {
        Page<AbilityDTO> page = abilityService.getAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add(Constant.TOTAL_COUNT, String.valueOf(page.getTotalElements()));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping()
    public ResponseEntity<Object> postAbility(@RequestBody AbilityDTO dto) {
        try {
            AbilityDTO res = abilityService.create(dto);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PutMapping()
    public ResponseEntity<Object> putAbility(@RequestBody AbilityDTO dto) {
        try {
            AbilityDTO res = abilityService.update(dto);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deleteAbility(@PathVariable("id") Long id) throws Exception {
        try {
            abilityService.delete(id);
            return ResponseEntity.ok("DELETE SUCCESS");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
