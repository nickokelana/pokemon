package com.example.pokemon.web.rest;

import com.example.pokemon.constant.Constant;
import com.example.pokemon.service.PokemonService;
import com.example.pokemon.service.dto.PokemonDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@Slf4j
@RestController
@RequestMapping("api/pokemons")
public class PokemonResource {

    @Autowired
    PokemonService pokemonService;

    @GetMapping()
    public ResponseEntity<Object> getPokemon(Pageable pageable) {
        Page<PokemonDTO> page = pokemonService.getAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add(Constant.TOTAL_COUNT, String.valueOf(page.getTotalElements()));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping()
    public ResponseEntity<Object> postPokemon(@RequestBody PokemonDTO dto) {
        try {
            PokemonDTO res = pokemonService.create(dto);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PutMapping()
    public ResponseEntity<Object> putPokemon(@RequestBody PokemonDTO dto) {
        try {
            PokemonDTO res = pokemonService.update(dto);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deletePokemon(@PathVariable("id") Long id) throws Exception {
        try {
            pokemonService.delete(id);
            return ResponseEntity.ok("DELETE SUCCESS");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
