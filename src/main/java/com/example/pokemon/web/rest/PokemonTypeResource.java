package com.example.pokemon.web.rest;

import com.example.pokemon.constant.Constant;
import com.example.pokemon.service.PokemonTypeService;
import com.example.pokemon.service.dto.PokemonTypeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("api/pokemon-types")
public class PokemonTypeResource {

    @Autowired
    PokemonTypeService pokemonTypeService;

    @GetMapping()
    public ResponseEntity<Object> getPokemonType(Pageable pageable) {
        Page<PokemonTypeDTO> page = pokemonTypeService.getAll(pageable);
        HttpHeaders headers = new HttpHeaders();
        headers.add(Constant.TOTAL_COUNT, String.valueOf(page.getTotalElements()));
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @PostMapping()
    public ResponseEntity<Object> postPokemonType(@RequestBody PokemonTypeDTO dto) {
        try {
            PokemonTypeDTO res = pokemonTypeService.create(dto);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PutMapping()
    public ResponseEntity<Object> putPokemonType(@RequestBody PokemonTypeDTO dto) {
        try {
            PokemonTypeDTO res = pokemonTypeService.update(dto);
            return ResponseEntity.ok(res);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> deletePokemonType(@PathVariable("id") Long id) throws Exception {
        try {
            pokemonTypeService.delete(id);
            return ResponseEntity.ok("DELETE SUCCESS");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
