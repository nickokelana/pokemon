package com.example.pokemon.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Entity
@Table(name = "ability")
public class Ability {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ability")
    private Long idAbility;

    private String description;

    @ToString.Exclude
    @ManyToMany(mappedBy = "abilities")
    Set<Pokemon> pokemons;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ability ability = (Ability) o;
        return Objects.equals(idAbility, ability.idAbility);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAbility);
    }
}
