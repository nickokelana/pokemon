package com.example.pokemon.service;

import com.example.pokemon.domain.PokemonType;
import com.example.pokemon.repository.PokemonTypeRepository;
import com.example.pokemon.service.dto.PokemonTypeDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class PokemonTypeService {
    @Autowired
    private PokemonTypeRepository pokemonTypeRepository;

    public Page<PokemonTypeDTO> getAll(Pageable pageable) {
        return pokemonTypeRepository.findAll(pageable).map(x -> new PokemonTypeDTO(x));
    }

    public Optional<PokemonType> checkName(String description) {
        return pokemonTypeRepository.findByName(description);
    }

    public PokemonTypeDTO create(PokemonTypeDTO dto) throws Exception {
        if (checkName(dto.getDescription()).isPresent()) {
            throw new Exception("Pokemon Type Already Exist!");
        }
        PokemonType pokemonType = pokemonTypeRepository.save(PokemonType.builder()
                        .description(dto.getDescription())
                .build());
        return new PokemonTypeDTO(pokemonType);
    }

    public PokemonTypeDTO update(PokemonTypeDTO dto) throws Exception {
        Optional<PokemonType> dataFromDB = pokemonTypeRepository.findById(dto.getId());
        if (dataFromDB.isEmpty()) {
            throw new Exception("Pokemon Type Not Found!");
        }
        Optional<PokemonType> exist = checkName(dto.getDescription());
        if (exist.isPresent()) {
            if (exist.get().getIdType() != dto.getIdType()) {
                throw new Exception("Pokemon Type Already Exist!");
            }
        }

        PokemonType pokemonType = dataFromDB.get();
        pokemonType.setDescription(dto.getDescription());
        pokemonType = pokemonTypeRepository.save(pokemonType);
        return new PokemonTypeDTO(pokemonType);
    }

    public void delete(Long id) {
        pokemonTypeRepository.deleteById(id);
    }
}
