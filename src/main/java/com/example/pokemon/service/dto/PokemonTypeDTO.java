package com.example.pokemon.service.dto;

import com.example.pokemon.domain.PokemonType;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PokemonTypeDTO {
    private Long id;
    private Long idType;
    private String description;
    public PokemonTypeDTO(PokemonType entity) {
        this.id = entity.getIdType();
        this.idType = entity.getIdType();
        this.description = entity.getDescription();
    }
}
