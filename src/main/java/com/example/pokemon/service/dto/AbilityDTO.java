package com.example.pokemon.service.dto;

import com.example.pokemon.domain.Ability;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class AbilityDTO {
    private Long id;
    private Long idAbility;
    private String description;
    public AbilityDTO(Ability entity) {
        this.id = entity.getIdAbility();
        this.idAbility = entity.getIdAbility();
        this.description = entity.getDescription();
    }
}
