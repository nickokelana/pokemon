package com.example.pokemon.service.dto;

import com.example.pokemon.domain.Pokemon;
import lombok.*;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PokemonDTO {
    private Long id;
    private Long idPokemon;
    private String name;
    private Integer height;
    private Integer weight;
    private Long pokemonTypeId;
    private String pokemonTypeDescription;
    private Set<AbilityDTO> abilities = new HashSet<>();
    public PokemonDTO(Pokemon entity) {
        this.id = entity.getId();
        this.idPokemon = entity.getId();
        this.name = entity.getName();
        this.height = entity.getHeight();
        this.weight = entity.getWeight();
        if (entity.getPokemonType() != null) {
            this.pokemonTypeId = entity.getPokemonType().getIdType();
            this.pokemonTypeDescription = entity.getPokemonType().getDescription();
        }
        this.abilities = entity.getAbilities().stream().map(x -> new AbilityDTO(x)).collect(Collectors.toSet());
    }
}
