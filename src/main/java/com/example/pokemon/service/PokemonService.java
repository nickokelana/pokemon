package com.example.pokemon.service;

import com.example.pokemon.domain.Ability;
import com.example.pokemon.domain.Pokemon;
import com.example.pokemon.domain.PokemonType;
import com.example.pokemon.repository.PokemonRepository;
import com.example.pokemon.service.dto.AbilityDTO;
import com.example.pokemon.service.dto.PokemonDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class PokemonService {
    @Autowired
    private PokemonRepository pokemonRepository;

    public Page<PokemonDTO> getAll(Pageable pageable) {
        return pokemonRepository.findAll(pageable).map(x -> new PokemonDTO(x));
    }

    public Optional<Pokemon> checkName(String name) {
        return pokemonRepository.findByName(name);
    }

    public PokemonDTO create(PokemonDTO dto) throws Exception {
        if (checkName(dto.getName()).isPresent()) {
            throw new Exception("Pokemon Already Exist!");
        }
        Pokemon pokemon = Pokemon.builder()
                .name(dto.getName())
                .height(dto.getHeight())
                .weight(dto.getWeight())
                .pokemonType(PokemonType.builder().idType(dto.getPokemonTypeId()).build())
                .build();
        Set<Ability> abilities = new HashSet<>();
        for(AbilityDTO each: dto.getAbilities()) {
            abilities.add(Ability.builder().idAbility(each.getIdAbility()).build());
        }
        pokemon.setAbilities(abilities);
        pokemon = pokemonRepository.save(pokemon);
        return new PokemonDTO(pokemon);
    }

    public PokemonDTO update(PokemonDTO dto) throws Exception {
        Optional<Pokemon> dataFromDB = pokemonRepository.findById(dto.getId());
        if (dataFromDB.isEmpty()) {
            throw new Exception("Pokemon Not Found!");
        }
        Optional<Pokemon> exist = checkName(dto.getName());
        if (exist.isPresent()) {
            if (exist.get().getId() != dto.getId()) {
                throw new Exception("Pokemon Already Exist!");
            }
        }

        Pokemon pokemon = dataFromDB.get();
        pokemon.setName(dto.getName());
        pokemon.setHeight(dto.getHeight());
        pokemon.setWeight(dto.getWeight());
        pokemon.setPokemonType(PokemonType.builder().idType(dto.getPokemonTypeId()).build());
        Set<Ability> abilities = new HashSet<>();
        for(AbilityDTO each: dto.getAbilities()) {
            abilities.add(Ability.builder().idAbility(each.getIdAbility()).build());
        }
        pokemon.setAbilities(abilities);
        pokemon = pokemonRepository.save(pokemon);
        return new PokemonDTO(pokemon);
    }

    public void delete(Long id) {
        pokemonRepository.deleteById(id);
    }
}
