package com.example.pokemon.service;

import com.example.pokemon.domain.Ability;
import com.example.pokemon.repository.AbilityRepository;
import com.example.pokemon.service.dto.AbilityDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class AbilityService {
    @Autowired
    private AbilityRepository abilityRepository;

    public Page<AbilityDTO> getAll(Pageable pageable) {
        return abilityRepository.findAll(pageable).map(x -> new AbilityDTO(x));
    }

    public Optional<Ability> checkName(String description) {
        return abilityRepository.findByName(description);
    }

    public AbilityDTO create(AbilityDTO dto) throws Exception {
        if (checkName(dto.getDescription()).isPresent()) {
            throw new Exception("Ability Already Exist!");
        }
        Ability ability = abilityRepository.save(Ability.builder()
                        .description(dto.getDescription())
                .build());
        return new AbilityDTO(ability);
    }

    public AbilityDTO update(AbilityDTO dto) throws Exception {
        Optional<Ability> dataFromDB = abilityRepository.findById(dto.getId());
        if (dataFromDB.isEmpty()) {
            throw new Exception("Ability Not Found!");
        }
        Optional<Ability> exist = checkName(dto.getDescription());
        if (exist.isPresent()) {
            if (exist.get().getIdAbility() != dto.getIdAbility()) {
                throw new Exception("Ability Already Exist!");
            }
        }

        Ability ability = dataFromDB.get();
        ability.setDescription(dto.getDescription());
        ability = abilityRepository.save(ability);
        return new AbilityDTO(ability);
    }

    public void delete(Long id) {
        abilityRepository.deleteById(id);
    }
}
