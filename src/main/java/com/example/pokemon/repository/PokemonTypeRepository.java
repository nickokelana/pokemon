package com.example.pokemon.repository;

import com.example.pokemon.domain.Ability;
import com.example.pokemon.domain.PokemonType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PokemonTypeRepository extends JpaRepository<PokemonType, Long> {
    @Query("select e from PokemonType e where e.description = :description")
    Optional<PokemonType> findByName(@Param("description") String description);
}
