package com.example.pokemon.repository;

import com.example.pokemon.domain.Ability;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AbilityRepository extends JpaRepository<Ability, Long> {
    @Query("select e from Ability e where e.description = :description")
    Optional<Ability> findByName(@Param("description") String description);
}
