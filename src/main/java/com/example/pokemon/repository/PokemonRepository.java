package com.example.pokemon.repository;

import com.example.pokemon.domain.Pokemon;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PokemonRepository extends JpaRepository<Pokemon, Long> {
    @Query("select e from Pokemon e where e.name = :name")
    Optional<Pokemon> findByName(@Param("name") String name);
}
